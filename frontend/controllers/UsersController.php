<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\rest\ActiveController;
use yii\filters\VerbFilter;
use backend\models\Users;
use common\models\LoginForm;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

/**
 * Site controller
 */
class UsersController extends ActiveController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\models\Users';

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'except' => ['login','create'],
                'only' => ['logout','signout','view','update'],
                'authMethods' => [
                    HttpBasicAuth::className(),
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return ['massege'=>'hayo ngapain? :v'];
    }

    public function actionView()
    {
        $model = \Yii::$app->user->identity;
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = password_hash($model->password, PASSWORD_DEFAULT);
            if($model->save()){
                return ['message' => 'register complete'];
            }else{
                return ['message' => 'register failed'];
            }
        }
        return "back away!";
    }
    public function actionUpdate()
    {
        // mengambil data user yang sendang login
        $user = \Yii::$app->user->identity;

        $model = $this->findModelId($user->id);

        if ($model->load(Yii::$app->request->post())) {

            // bila user tidak mengedit password
            if(empty(@$model->password) || $model->password == 0 || $model->password == null){
                $model->password = $user->password;
            }else{
                $model->password = password_hash($model->password, PASSWORD_DEFAULT);            
            }
            // menyimpan
            if($model->save()){
                return ['message' => 'update complete'];
            }else{
                return ['message' => 'update failed'];
            }
        }
        return "back away!";
    }
    public function actionLogout()
    {
        $logout = Yii::$app->user->logout();
        if($logout){
            return ['message' => 'logout success'];
        }else{
            return ['message' => 'logout failed'];
        }
    }
    public function actionLogin()
    {
        // check login
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $username = $model->username;
            $password = $model->password;
            // make auth key for android
            $authKey = hash('md5',$password . rand(1,10));

            $model = $this->findModel($username);
            $model->authKey = $authKey;
            if ($model->update()) {
                return $authKey;
            }
            return "non";
        } else {
            return ['messege' => "login error"];
        }
        return ['messege' => "please post you username and password"];
    }
    public function actionOut()
    {
        $model = $this->findModelId(\Yii::$app->user->identity->id);
        $model->authKey = '';
        if ($model->update()) {
            return ['messege'=>'logout success'];
        }
        return ['messege'=>'logout failed'];
    }
    protected function findModelId($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModel($username)
    {
        if (($model = Users::findOne(['username'=>$username])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
