<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "device".
 *
 * @property integer $device_id
 * @property string $device_uuid
 * @property string $device_name
 * @property integer $users_id
 * @property string $device_created_at
 * @property string $device_updated_at
 *
 * @property Users $users
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_uuid', 'device_name', 'users_id'], 'required'],
            [['users_id'], 'integer'],
            [['device_created_at', 'device_updated_at'], 'safe'],
            [['device_uuid', 'device_name'], 'string', 'max' => 100],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'device_uuid' => 'Device Uuid',
            'device_name' => 'Device Name',
            'users_id' => 'Users ID',
            'device_created_at' => 'Device Created At',
            'device_updated_at' => 'Device Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }
}
