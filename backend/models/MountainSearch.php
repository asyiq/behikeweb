<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Mountain;

/**
 * MountainSearch represents the model behind the search form about `backend\models\Mountain`.
 */
class MountainSearch extends Mountain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mountain_id'], 'integer'],
            [['mountain_name', 'mountain_address', 'mountain_height', 'mountain_image', 'mountain_description', 'mountain_created_at', 'mountain_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mountain::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mountain_id' => $this->mountain_id,
            'mountain_created_at' => $this->mountain_created_at,
            'mountain_updated_at' => $this->mountain_updated_at,
        ]);

        $query->andFilterWhere(['like', 'mountain_name', $this->mountain_name])
            ->andFilterWhere(['like', 'mountain_address', $this->mountain_address])
            ->andFilterWhere(['like', 'mountain_height', $this->mountain_height])
            ->andFilterWhere(['like', 'mountain_image', $this->mountain_image])
            ->andFilterWhere(['like', 'mountain_description', $this->mountain_description]);

        return $dataProvider;
    }
}
