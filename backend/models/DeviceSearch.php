<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Device;

/**
 * DeviceSearch represents the model behind the search form about `backend\models\Device`.
 */
class DeviceSearch extends Device
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_id', 'users_id'], 'integer'],
            [['device_uuid', 'device_name', 'device_created_at', 'device_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Device::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'device_id' => $this->device_id,
            'users_id' => $this->users_id,
            'device_created_at' => $this->device_created_at,
            'device_updated_at' => $this->device_updated_at,
        ]);

        $query->andFilterWhere(['like', 'device_uuid', $this->device_uuid])
            ->andFilterWhere(['like', 'device_name', $this->device_name]);

        return $dataProvider;
    }
}
