<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mountain".
 *
 * @property integer $mountain_id
 * @property string $mountain_name
 * @property string $mountain_address
 * @property string $mountain_height
 * @property string $mountain_image
 * @property string $mountain_description
 * @property string $mountain_created_at
 * @property string $mountain_updated_at
 */
class Mountain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mountain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mountain_image'], 'required'],
            [['mountain_description'], 'string'],
            [['mountain_created_at', 'mountain_updated_at'], 'safe'],
            [['mountain_name'], 'string', 'max' => 45],
            [['mountain_address', 'mountain_image'], 'string', 'max' => 255],
            [['mountain_height'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mountain_id' => 'Mountain ID',
            'mountain_name' => 'Mountain Name',
            'mountain_address' => 'Mountain Address',
            'mountain_height' => 'Mountain Height',
            'mountain_image' => 'Mountain Image',
            'mountain_description' => 'Mountain Description',
            'mountain_created_at' => 'Mountain Created At',
            'mountain_updated_at' => 'Mountain Updated At',
        ];
    }
}
