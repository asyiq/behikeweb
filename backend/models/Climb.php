<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "climb".
 *
 * @property integer $climb_id
 * @property integer $user_id
 * @property integer $climb_mountain_id
 * @property string $climb_created_at
 * @property string $climb_updated_at
 *
 * @property Users $user
 * @property Mountain $climbMountain
 */
class Climb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'climb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'climb_mountain_id'], 'required'],
            [['user_id', 'climb_mountain_id'], 'integer'],
            [['climb_created_at', 'climb_updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['climb_mountain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mountain::className(), 'targetAttribute' => ['climb_mountain_id' => 'mountain_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'climb_id' => 'Climb ID',
            'user_id' => 'User ID',
            'climb_mountain_id' => 'Climb Mountain ID',
            'climb_created_at' => 'Climb Created At',
            'climb_updated_at' => 'Climb Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClimbMountain()
    {
        return $this->hasOne(Mountain::className(), ['mountain_id' => 'climb_mountain_id']);
    }
}
