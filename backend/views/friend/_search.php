<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FriendSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="friend-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'friend_id') ?>

    <?= $form->field($model, 'users_id') ?>

    <?= $form->field($model, 'friend_created_at') ?>

    <?= $form->field($model, 'friend_updated_at') ?>

    <?= $form->field($model, 'id_friend') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
