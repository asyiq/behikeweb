<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Friend */

$this->title = $model->friend_id;
$this->params['breadcrumbs'][] = ['label' => 'Friends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="friend-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'friend_id' => $model->friend_id, 'id_friend' => $model->id_friend], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'friend_id' => $model->friend_id, 'id_friend' => $model->id_friend], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'friend_id',
            'users_id',
            'friend_created_at',
            'friend_updated_at',
            'id_friend',
        ],
    ]) ?>

</div>
