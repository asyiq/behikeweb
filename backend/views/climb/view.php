<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Climb */

$this->title = $model->climb_id;
$this->params['breadcrumbs'][] = ['label' => 'Climbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="climb-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->climb_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->climb_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'climb_id',
            'user_id',
            'climb_mountain_id',
            'climb_created_at',
            'climb_updated_at',
        ],
    ]) ?>

</div>
