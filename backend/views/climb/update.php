<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Climb */

$this->title = 'Update Climb: ' . $model->climb_id;
$this->params['breadcrumbs'][] = ['label' => 'Climbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->climb_id, 'url' => ['view', 'id' => $model->climb_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="climb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
