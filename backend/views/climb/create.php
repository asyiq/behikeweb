<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Climb */

$this->title = 'Create Climb';
$this->params['breadcrumbs'][] = ['label' => 'Climbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="climb-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
