<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Mountain */

$this->title = 'Update Mountain: ' . $model->mountain_id;
$this->params['breadcrumbs'][] = ['label' => 'Mountains', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mountain_id, 'url' => ['view', 'id' => $model->mountain_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mountain-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
