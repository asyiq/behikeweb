<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MountainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mountain-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'mountain_id') ?>

    <?= $form->field($model, 'mountain_name') ?>

    <?= $form->field($model, 'mountain_address') ?>

    <?= $form->field($model, 'mountain_height') ?>

    <?= $form->field($model, 'mountain_image') ?>

    <?php // echo $form->field($model, 'mountain_description') ?>

    <?php // echo $form->field($model, 'mountain_created_at') ?>

    <?php // echo $form->field($model, 'mountain_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
