<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Mountain */

$this->title = 'Create Mountain';
$this->params['breadcrumbs'][] = ['label' => 'Mountains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mountain-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
